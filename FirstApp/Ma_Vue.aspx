﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ma_Vue.aspx.cs" Inherits="FirstApp.Ma_Vue" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="StyleSheet1.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <div id="page">
            <header></header>
            <nav></nav>
            <h1>Bienvenue sur Planet Gamers</h1>
            <p>
                <a href="Ma_Vue.aspx">Planet Gamers </a>est un site Web communautaire basé sur l&#39;univers des<span class="auto-style"> jeux vidéos.</span></p>
            <p>
                Vous souhaitez présete un jeu, donner une critique ou tout simplement discuter ? Planet Gamers est fait pour vous !</p>
            <p>
                Nous vous proposons articles, vidéos et forums. N&#39;hésitez pas à participer à la vie de cette communauté.</p>
        </div>
    </form>
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">Une liste non ordonnée</td>
            <td>
                <ul>
                    <li>C#</li>
                    <li>ASP.NET</li>
                    <li>site Web</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Une liste ordonnée</td>
            <td>
                <ol>
                    <li>Apprendre C#</li>
                    <li>Apprendre ASP.NET</li>
                    <li>Faire mon site web</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Un controle ASP.NET</td>
            <td class="auto-style4">
                
            </td>
        </tr>
    </table>
</body>
</html>
