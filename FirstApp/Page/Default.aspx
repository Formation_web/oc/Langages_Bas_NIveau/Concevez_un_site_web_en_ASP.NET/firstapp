﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstApp.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Bienvenue sur mon site web !<br />
            Ceci est ma premiere page avec ASP.NET; alors soyez indulgent ! :)<br />
        </div>

        
        <asp:TextBox ID="Txt_saisie" runat="server" Height="16px"></asp:TextBox>
        <asp:Button ID="btn_valider" runat="server" Height="27px" Text="Valider" BackColor="#3366FF" BorderColor="Red" BorderStyle="Double" BorderWidth="5px" Font-Size="15px" ForeColor="#FFFFCC" OnClick="btn_valider_Click" />
        
    </form>
    <asp:label runat="server" text="Label" ID="Lbl_resultat"></asp:label>
</body>
</html>

